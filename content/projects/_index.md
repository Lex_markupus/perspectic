{
  "title": "Our projects",
  "description": "Lorem ipsum dolar sit amet",
  "date": "2018-01-01T00:00:00Z",
  "projects_banner": {
    "title": "Our projects",
    "description": "We take pride in simplifying our pricing process. We got rid of the “Get a Quote” curse once and for all. Simple approach, great result."
  },
  "projects_row": [
    {
      "title": "Beautiful project",
      "description": "Short Description",
      "image": "/uploads/12_roda_atmosphere_5.jpg"
    },
    {
      "title": "Project name",
      "description": "Short Description",
      "image": "/uploads/17_siracusa_atmosphere_7.jpg"
    },
    {
      "title": "Project name",
      "image": "/uploads/12_roda_atmosphere_3.jpg",
      "description": "Short Description"
    },
    {
      "title": "Project name",
      "description": "Short Description",
      "image": "/uploads/napoli_12.jpg"
    }
  ],
  "menu": {
    "main": {
      "identifier": "Our projects",
      "weight": 4
    }
  }
}
Duro di erat, dolorem victrices, manu plus fatis despice.