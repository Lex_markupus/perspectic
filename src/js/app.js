import "../scss/app.scss";
import "aos/dist/aos.css";
import "swiper/swiper-bundle.css";

import "../js/action";

import AOS from "aos";

AOS.init();

window.onload = (event) => console.log("Loaded!", { event });
