{
  "title": "Home",
  "description": "Perspectic description",
  "date": "2018-01-01T00:00:00Z",
  "home_banner": {
    "banner_title": "We make digital furniture. For real.",
    "banner_description": "Simple 3D modeling service that would help you in generating top-quality models for your product catalogs or create whole scenes that you need to illustrate your work ideas.",
    "banner_image": "/uploads/bnr-img.png",
    "banner_main_title": "<p>We make digital furniture. <br><strong>For real.</strong></p>"
  },
  "section_how": {
    "top_row": [
      {
        "title": "How do we help?",
        "description": "We help to completely eliminate the need to maintain expensive photography capacity for your furniture production or a store. We are happy to step in."
      },
      {
        "title": "How does this work?",
        "description": "We help to completely eliminate the need to maintain expensive photography capacity for your furniture production or a store. We are happy to step in."
      },
      {
        "title": "What do I do?",
        "description": "We help to completely eliminate the need to maintain expensive photography capacity for your furniture production or a store. We are happy to step in."
      }
    ],
    "bottom_row": [
      {
        "media": "/uploads/pricing-2.png",
        "animation_name": "fade-right"
      },
      {
        "media": "/uploads/pricing-2.png",
        "animation_name": "fade-up"
      },
      {
        "media": "/uploads/pricing-2.png",
        "animation_name": "fade-left"
      }
    ],
    "upload_link": "test link"
  },
  "section_what": {
    "title": "What can you expect?",
    "media_row": [
      {
        "image": "/uploads/square2.jpg"
      },
      {
        "image": "/uploads/square2.jpg"
      }
    ],
    "text_row": [
      {
        "title": "Untextured Models",
        "description": "We create great bare-bones models, which are easy to use and modify. "
      },
      {
        "title": "Textured Models",
        "description": "You can have your models change with variety of proportionally sized textures, which would allow to easily catalog and use in multiple applications across your business."
      }
    ],
    "brand_logos_row": [
      {
        "image": "/uploads/1.png"
      },
      {
        "image": "/uploads/2.png"
      },
      {
        "image": "/uploads/3.png"
      },
      {
        "image": "/uploads/4.png"
      },
      {
        "image": "/uploads/5.png"
      },
      {
        "image": "/uploads/6.png"
      }
    ],
    "options_row": [
      {
        "title": "Quality Results",
        "description": "We believe in quality, reliability and simplicity. By providing a service we wouldn’t mind using ourselves, we deliver much needed component to our clients’ 3D visualization strategies."
      },
      {
        "title": "Experienced",
        "description": "We have been working with 3D tools in variety of fields for many years. Having our roots in design, architecture and adjacent fields – brings tremendous amount of skill and experience to get your models just right."
      }
    ]
  },
  "menu": {
    "main": {
      "identifier": "home",
      "weight": 1
    }
  }
}
