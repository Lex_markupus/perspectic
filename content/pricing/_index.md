{
  "title": "Pricing",
  "description": "Lorem ipsum dolar sit amet",
  "date": "2018-01-01T00:00:00Z",
  "about_banner": {
    "title": "",
    "description": ""
  },
  "about_row": [

  ],
  "pricing_banner": {
    "title": "How much does this cost?",
    "description": "We take pride in simplifying our pricing process. We got rid of the “Get a Quote” curse once and for all. Simple approach, great result."
  },
  "pricing_row": [
    {
      "title": "Single model.",
      "description": "Including textile/finish variations.",
      "price": "$99",
      "price_description": "Any complexity. Unlimited variations. Guaranteed.",
      "omage": "/uploads/pricing-1.png",
      "image_media": "/uploads/pricing-1.png"
    },
    {
      "title": "Custom Scene",
      "description": "Including 5 angles renderings.",
      "price": "$299",
      "price_description": "Per beatifully matching custom scene.",
      "omage": "/uploads/square2.jpg",
      "image_media": ""
    }
  ],
  "media_pricing_block": {
    "image_large": "/uploads/carrie-chair-d5.jpg",
    "image_small_top": "/uploads/carrie-chair-d5.jpg",
    "image_small_bottom": "/uploads/carrie-chair-d5.jpg"
  },
  "menu": {
    "main": {
      "identifier": "pricing",
      "weight": 2
    }
  }
}
Duro di erat, dolorem victrices, manu plus fatis despice. Consensistis [ceratis
et at](http://senectarenuente.org/) quam vulnere volanti, et remos. Inpositum
naturae hic linguae, et misit ecce durataeque dextra, per.