{
  "title": "About Us",
  "description": "Lorem ipsum dolar sit amet",
  "date": "2018-01-01T00:00:00Z",
  "about_banner": {
    "title": "About Us",
    "description": "We take pride in simplifying our pricing process. We got rid of the “Get a Quote” curse once and for all. Simple approach, great result."
  },
  "about_row": [
    {
      "title": "United States",
      "description": "Perspectic is headquartered in Princeton, New Jersey which helps us to stain in touch with North American operations."
    },
    {
      "title": "United Kingdom",
      "description": "European operations are managed from London, UK with convenient timezone support and proximity."
    }
  ],
  "menu": {
    "main": {
      "identifier": "about",
      "weight": 3
    }
  }
}
Duro di erat, dolorem victrices, manu plus fatis despice.