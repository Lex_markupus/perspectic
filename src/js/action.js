$(document).ready(function () {
	$(".j-toggle-nav").on("click", function (e) {
		e.preventDefault();
		$("html").toggleClass("open-nav");
	});
});

var blockHeight = $(".section-banner").innerHeight();

$(window).scroll(function () {

	var newLoc = $(document).scrollTop();
	var rotation = (newLoc / blockHeight) * 100;

	var rotationStr = "rotateX(" + rotation + "deg) scaleZ(1)";
	$(".section-banner").css({
		"-webkit-transform": rotationStr,
		"-moz-transform": rotationStr,
		transform: rotationStr,
	});
});
